'''
Diagramme de classe: UML
----------------------------------------------------
CompteSimple

Titulaire: Personne
Solde: Int

crediter(self,montant)
debiter(self,montant)

__init__(self,titulaire,depotInitial)

----------------------------------------------------
CompteCourant hérite de CompteSimple

operations : Liste de String

editerRelever()

__init__(self, titulaire,depotInitial)
crediter(self,montant)
debiter(self,montant)
'''

'''
Ne pas oublier les tests (en module dans un autre fichier avec pytest)
'''


    
class CompteSimple:
    '''
    attributs : 
    Titulaire: Personne
    Solde: Int

    methodes : 
    crediter(self,montant)
    debiter(self,montant)
    '''
    __identifiant= 10000
    
    def __init__(self,titulaire, depotInitial):
        CompteSimple.__identifiant += 1
        self.titulaire = titulaire
        self.__solde = depotInitial
        self.id= CompteSimple.__identifiant
        

    def crediter(self,montant):
        self.__solde +=  montant 

    def debiter(self,montant):
        self.__solde -= montant

    @property
    def solde(self):
        return self.__solde




class Banque:
    '''
    attributs:
    Compte: compte
    Titulaire: str

    methodes:
    init__()
    open_account()
    total_solde()
    prelever()  
    '''
    
    def __init__(self,frais):
        self.comptes= []        
        self.frais= frais

    def ouvrir_compte(self, titulaire, solde):
        self.comptes.append(CompteSimple(titulaire,solde) )
        self.solde= solde

    def ouvrir_compte_courant(self, titulaire, solde):
        self.comptes.append(CompteCourant(titulaire,solde) )
        self.solde= solde


    def somme_all_soldes(self):
        total_solde = 0
        for c in self.comptes :
            total_solde += c.solde
        return total_solde


    def prelever(self):
        for c in self.comptes :
            c.debiter(self.frais)

    def editer(self):
        for c in self.comptes:
            if isinstance(c,CompteCourant):
                c.editer()

   
        


class CompteCourant(CompteSimple): 
    '''
    attributs:
    tous les attributs de compteSimple
    operations : liste contenant les opérations effectuées

    methodes:
    toutes les méthodes de compteSimple
    editerRelever()
    '''

    def __init__(self, titulaire, depotInitial):
        CompteSimple.__init__(self, titulaire, depotInitial) 
        self.operations = ["Ajout de " + str(depotInitial) +" euros"]
   
    def crediter(self,montant):
        CompteSimple.crediter(self,montant) 
        self.operations.append("Ajout de " + str(montant) + " euros") 

    def debiter(self,montant):
        CompteSimple.debiter(self,montant)
        self.operations.append("Debit de " + str(montant)+ " euros")

    
    def editer(self):
        print("Relevé :")
        for operation in self.operations:
            print(operation)


    

