from banques import CompteSimple 
from banques import CompteCourant
from banques import Banque

import pytest

def test_compte_simple():

    nouveau_compte= CompteSimple("Jean", 100)
    print(nouveau_compte.solde)

    nouveau_compte.debiter(50)
    assert nouveau_compte.solde == 50
    print(nouveau_compte.solde)


    nouveau_compte.crediter(50)
    assert nouveau_compte.solde == 100
    print(nouveau_compte.solde)

if __name__ == '__main__':
    test_compte_simple()


def test_compte_courant():
    compte_courant = CompteCourant("Jean", 100)
    print(compte_courant.solde)
    print(compte_courant.operations)
    assert compte_courant.solde == 100
    assert compte_courant.operations == ["Ajout de 100 euros"]

    compte_courant.debiter(50)
    assert compte_courant.solde == 50
    assert compte_courant.operations == ["Ajout de 100 euros","Debit de 50 euros"]
    print(compte_courant.solde)

    compte_courant.editer()

if __name__ == "__main__":
    test_compte_courant()



def test_banque():
    banque= Banque(10)
    banque.ouvrir_compte("Jean", 100)
    banque.ouvrir_compte_courant("Marc", 200)
    banque.somme_all_soldes()
    assert banque.somme_all_soldes()== 300

if __name__ == "__main__":
    test_banque()

